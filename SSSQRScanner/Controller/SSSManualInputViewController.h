//
//  SSSManualInputViewController.h
//  SSSQRScanner
//
//  Created by El Desperado on 31/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSSManualInputViewController : UIViewController<UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet UIScrollView* scrollView;
@property(strong, nonatomic) IBOutlet UITextField* codeInputTextField;
@property(strong, nonatomic) IBOutlet UIButton* sendBTN;
- (IBAction)bgTappedAction:(id)sender;
- (IBAction)sendCodeAction:(id)sender;

@end
