//
//  User.m
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "User.h"

@implementation User

@dynamic userId;
@dynamic username;
@dynamic email;

@end
