//
//  User.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@interface User : NSManagedObject

@property(nonatomic, retain) NSNumber* userId;
@property(nonatomic, retain) NSString* username;
@property(nonatomic, retain) NSString* email;

@end
