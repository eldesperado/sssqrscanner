//
//  NSDictionary+Verified.m
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "NSDictionary+Verified.h"

@implementation NSDictionary (Verified)
/**
 *  Check whether object with corresponding key in NSDictionary is null or empty
 *or not
 *
 *  @param aKey The key for which to return the corresponding value
 *
 *  @return The value associated with aKey, or nil if no value is associated
 *with aKey
 */
- (id)verifiedObjectForKey:(id)aKey {
  if ([self objectForKey:aKey] &&
      ![[self objectForKey:aKey] isKindOfClass:[NSNull class]])
    return [self objectForKey:aKey];
  return nil;
}
@end
