//
//  SSSMessageModalViewController.m
//  SSSQRScanner
//
//  Created by El Desperado on 31/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "SSSMessageModalViewController.h"
#import "AppDelegate.h"

@interface SSSMessageModalViewController () {
  UIViewController* scanVC;
}
@end

@implementation SSSMessageModalViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  if (self.isSucceed != nil) {
    if ([self.isSucceed boolValue]) {
      [self displaySuccessMessage];
    } else if (self.message != nil) {
      [self displayErrorWithMessage:self.message];
    }
  }
  scanVC = [[UIStoryboard
      storyboardWithName:@"Main"
                  bundle:[NSBundle
                                 mainBundle]] instantiateInitialViewController];
}

- (void)displaySuccessMessage {
  [self.statusLabel setText:@"VALID"];
  [self.iconImageView setImage:[UIImage imageNamed:@"success"]];
  [self.messageLabel setHidden:YES];
  [self.view
      setBackgroundColor:
          [UIColor colorWithRed:0.451 green:0.788 blue:0.420 alpha:1.000]];
}

- (void)displayErrorWithMessage:(NSString*)message {
  [self.statusLabel setText:@"INVALID"];
  [self.iconImageView setImage:[UIImage imageNamed:@"error"]];
  [self.messageLabel setHidden:NO];
  [self.messageLabel setText:message];
  [self.view
      setBackgroundColor:
          [UIColor colorWithRed:0.753 green:0.224 blue:0.169 alpha:1.000]];
}

- (IBAction)didTappedAction:(id)sender {
  AppDelegate* appDelegateTemp = [[UIApplication sharedApplication] delegate];
  appDelegateTemp.window.rootViewController = scanVC;
}
@end
