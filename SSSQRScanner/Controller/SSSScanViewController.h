//
//  SSSScanViewController.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface SSSScanViewController
    : UIViewController<AVCaptureMetadataOutputObjectsDelegate>

@property(weak, nonatomic) IBOutlet UIView* viewPreview;
@property(weak, nonatomic) IBOutlet UIBarButtonItem* manualMethodBBItem;
@property(weak, nonatomic) IBOutlet UIBarButtonItem* settingBBItem;
@property(weak, nonatomic) IBOutlet UIToolbar* toolbar;
@property(weak, nonatomic) IBOutlet UILabel* guideTipLabel;

- (BOOL)isCameraAvailable;
- (void)startScanning;
- (void)stopScanning;

@end