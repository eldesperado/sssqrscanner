//
//  SSSLoginViewController.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSSLoginViewController : UIViewController<UITextFieldDelegate>
@property(strong, nonatomic) IBOutlet UIScrollView* scrollView;
@property(strong, nonatomic) IBOutlet UITextField* usernameTextField;
@property(strong, nonatomic) IBOutlet UITextField* passwTextField;
@property(strong, nonatomic) IBOutlet UIButton* signInBTN;
- (IBAction)signInBTNTappedAction:(id)sender;

- (IBAction)bgTappedAction:(id)sender;
@end
