//
//  AuthenticationClient.m
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "AuthenticationClient.h"
#import "AccessToken.h"
#import "SSSWebService.h"
#import "NSDictionary+Verified.h"

@interface AuthenticationClient ()
@property(nonatomic, copy) NSURL* baseUrl;
@property(nonatomic, copy) NSURLSession* client;
@end

@implementation AuthenticationClient
@synthesize persistent = _persistent;
#pragma mark -
#pragma mark - Singleton Instance
+ (id)sharedInstance {
  static AuthenticationClient* sharedInstance = nil;
  static dispatch_once_t onceToken;
  dispatch_once(&onceToken, ^{
    sharedInstance = [[self alloc] init];
  });
  return sharedInstance;
}

#pragma mark -
#pragma mark - Accessors
- (void)setPersistent:(BOOL)shouldPersist {
  if (_persistent == shouldPersist) {
    return;
  }

  if (shouldPersist && accessToken) {
    [self.accessToken storeInDefaultKeychain];
  }

  if (_persistent && !shouldPersist) {
    [self.accessToken removeFromDefaultKeychain];
  }
  [self willChangeValueForKey:@"persistent"];
  _persistent = shouldPersist;
  [self didChangeValueForKey:@"persistent"];
}

- (AccessToken*)accessToken {
  if (accessToken) {
    return accessToken;
  }

  if (_persistent) {
    // Get Saved Token from Keychain
    accessToken = [AccessToken tokenFromDefaultKeychain];
    // Delegate
    return accessToken;
  } else {
    return nil;
  }
}

- (void)setAccessToken:(AccessToken*)value {
  if (self.accessToken == value) {
    return;
  }

  if (!value) {
    [self.accessToken removeFromDefaultKeychain];
    self.isAuthenticated = NO;
  }

  [self willChangeValueForKey:@"accessToken"];
  accessToken = value;
  [self didChangeValueForKey:@"accessToken"];

  if (_persistent) {
    // Save Token to Keychain
    [accessToken storeInDefaultKeychain];
    self.isAuthenticated = YES;
  }
}

#pragma mark -
#pragma mark - Initialize
- (id)init {
  if (self = [super init]) {
    self.persistent = YES;
    self.accessToken = [self accessToken];
    self.client = [[SSSWebService sharedService] client];
    self.baseUrl = [[SSSWebService sharedService] baseUrl];
    if (self.accessToken) {
      self.isAuthenticated = YES;
    } else {
      self.isAuthenticated = NO;
    }
  }
  return self;
}

- (id)initWithPersistent:(BOOL)shouldPersist {
  self = [super init];
  if (self) {
    self.persistent = shouldPersist;
    self.client = [[SSSWebService sharedService] client];
    self.baseUrl = [[SSSWebService sharedService] baseUrl];
    self.isAuthenticated = NO;
  }
  return self;
}

#pragma mark -
#pragma mark - Check Login
- (BOOL)isLoggedIn {
  User* currentUser = [User MR_findFirst];
  BOOL isHadAccessToken = [self isHadAccessToken];
  BOOL isSetUserDefault = [[NSUserDefaults standardUserDefaults]
      boolForKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  BOOL authenticatedUser = currentUser && isSetUserDefault && isHadAccessToken;
  return authenticatedUser;
}
- (BOOL)isHadAccessToken {
  return self.isAuthenticated;
}

#pragma mark -
#pragma mark - Request Token
- (RACSignal*)authenticateWithUsername:(NSString*)username
                              password:(NSString*)password {
  RACSignal* authenticateSignal =
      [self getAccessTokenWithUsername:username password:password];
  RACSignal* getTokenSignal = [self tokenForSignal:authenticateSignal];
  return [RACSignal createSignal:^RACDisposable*(id<RACSubscriber> subscriber) {
    [getTokenSignal subscribeNext:^(AccessToken* token) {
      // If get Access Token from Server successfully, get User Profile
      // and save into Core Data
      if (token && [token isKindOfClass:[AccessToken class]]) {
        [self setAccessToken:token];
        [[NSUserDefaults standardUserDefaults]
            setBool:YES
             forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [subscriber sendNext:@(YES)];
        [subscriber sendCompleted];
      } else {
        NSError* error = (NSError*)token;
        [subscriber sendError:error];
      }
    } error:^(NSError* error) {
      [subscriber sendError:error];
    }];
    return [RACDisposable disposableWithBlock:^{
        //
    }];
  }];
}

- (RACSignal*)getAccessTokenWithUsername:(NSString*)username
                                password:(NSString*)password {
  NSString* relativeString = @"login";

  return [RACSignal createSignal:^RACDisposable*(id<RACSubscriber> subscriber) {

    NSMutableURLRequest* request =
        [NSMutableURLRequest requestWithURL:[NSURL URLWithString:relativeString
                                                   relativeToURL:self.baseUrl]];

    NSString* paramString = [NSString
        stringWithFormat:@"username=%@&password=%@", username, password];
    [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];

    NSURLSessionDataTask* task = [self.client
        dataTaskWithRequest:request
          completionHandler:^(NSData* data, NSURLResponse* response,
                              NSError* error) {
            if (error) {
              [subscriber sendError:error];
            } else if (!data) {
              NSDictionary* userInfo = @{
                NSLocalizedDescriptionKey :
                    @"No data was received from the server."
              };
              NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                   code:ERROR_CODE
                                               userInfo:userInfo];
              [subscriber sendError:error];
            } else {
              NSError* jsonError;
              NSDictionary* dict = [NSJSONSerialization
                  JSONObjectWithData:data
                             options:NSJSONReadingMutableContainers
                               error:&jsonError];

              if (jsonError) {
                [subscriber sendError:jsonError];
              } else {
                [subscriber sendNext:dict];
                [subscriber sendCompleted];
              }
            }
          }];
    [task resume];

    return [RACDisposable disposableWithBlock:^{
      [task cancel];
    }];
  }];
}

- (RACSignal*)tokenForSignal:(RACSignal*)signal {
  return [signal map:^id(NSDictionary* response) {
    if (![response verifiedObjectForKey:@"error"]) {
      NSManagedObjectContext* localContext =
          [NSManagedObjectContext MR_contextForCurrentThread];
      User* user = [User MR_createInContext:localContext];
      [localContext performBlockAndWait:^{
        [user MR_importValuesForKeysWithObject:response];
        // Save to Core Data
        [localContext MR_saveToPersistentStoreAndWait];
      }];

      AccessToken* token = [AccessToken tokenWithResponseBody:response];
      return token;
    } else {
      NSString* errorDesc = [[response verifiedObjectForKey:@"error"]
          verifiedObjectForKey:@"message"];
      NSError* error;
      if (errorDesc) {
        NSDictionary* userInfo = @{NSLocalizedDescriptionKey : errorDesc};
        error = [NSError errorWithDomain:ERROR_DOMAIN
                                    code:ERROR_CODE
                                userInfo:userInfo];

      } else {
        error =
            [NSError errorWithDomain:ERROR_DOMAIN code:ERROR_CODE userInfo:nil];
      }
      return error;
    }
  }];
}

#pragma mark -
#pragma mark - Logout
- (void)logout {
  // Remove Access Token
  [self setAccessToken:nil];

  // Truncate all User Information in "User" Entity in Core Date
  [User MR_truncateAll];
  [[NSManagedObjectContext MR_defaultContext] MR_saveToPersistentStoreAndWait];

  [[NSUserDefaults standardUserDefaults] setBool:NO
                                          forKey:NSUSERDEFAULTS_LOGGEDIN_KEY];
  [[NSUserDefaults standardUserDefaults] synchronize];
}
@end
