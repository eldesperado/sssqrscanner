//
//  AuthenticationClient.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa/ReactiveCocoa.h>

@class AccessToken;

@interface AuthenticationClient : NSObject {
 @protected
  BOOL persistent;
  AccessToken* accessToken;
}
@property(nonatomic, strong) AccessToken* accessToken;
@property(nonatomic) BOOL isAuthenticated;
/*!
 * If set to NO, the access token is not stored any keychain, will be removed if
 * it was.
 * Defaults to YES
 */
@property(nonatomic, assign, readwrite, getter=isPersistent) BOOL persistent;

+ (id)sharedInstance;

- (id)initWithPersistent:(BOOL)shouldPersist;
- (BOOL)isHadAccessToken;
- (BOOL)isLoggedIn;
/*!
 * Authenticate with username & password (User Credentials Flow)
 */
- (RACSignal*)authenticateWithUsername:(NSString*)username
                              password:(NSString*)password;
- (void)logout;
@end
