//
//  SSSWebService.m
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "SSSWebService.h"
#import "NSDictionary+Verified.h"
#import "ReactiveCocoa.h"
#import "RACEXTScope.h"
#import "AuthenticationClient.h"
#import "AccessToken.h"

@implementation SSSWebService

+ (SSSWebService*)sharedService {
  static SSSWebService* sharedService = nil;
  if (!sharedService) {
    sharedService = [[super allocWithZone:nil] init];
    sharedService.client = [NSURLSession
        sessionWithConfiguration:[NSURLSessionConfiguration
                                         defaultSessionConfiguration]
                        delegate:sharedService
                   delegateQueue:nil];
    sharedService.baseUrl = [NSURL URLWithString:WEB_SERVER_URL];
  }
  return sharedService;
}

+ (id)allocWithZone:(struct _NSZone*)zone {
  return [self sharedService];
}

- (id)init {
  if (self = [super init]) {
  }
  return self;
}

#
#pragma mark - Get Product Detail
- (RACSignal*)scanWithCode:(NSString*)code {
  NSString* relativeString = @"scan";
  RACSignal* signal = [RACSignal createSignal:^RACDisposable*(id<RACSubscriber>
                                                                  subscriber) {
    NSMutableURLRequest* request =
        [NSMutableURLRequest requestWithURL:[NSURL URLWithString:relativeString
                                                   relativeToURL:self.baseUrl]];
    NSString* accessToken =
        [[[AuthenticationClient sharedInstance] accessToken] accessToken];
    NSString* paramString = [NSString
        stringWithFormat:@"code=%@&access_token=%@", code, accessToken];
    [request setHTTPBody:[paramString dataUsingEncoding:NSUTF8StringEncoding]];
    [request setHTTPMethod:@"POST"];

    NSURLSessionDataTask* task = [self.client
        dataTaskWithRequest:request
          completionHandler:^(NSData* data, NSURLResponse* response,
                              NSError* error) {
            if (error) {
              [subscriber sendError:error];
            } else if (!data) {
              NSDictionary* userInfo = @{
                NSLocalizedDescriptionKey :
                    @"No data was received from the server."
              };
              NSError* error = [NSError errorWithDomain:ERROR_DOMAIN
                                                   code:ERROR_CODE
                                               userInfo:userInfo];
              [subscriber sendError:error];
            } else {
              // Get Response Status Code, if it equals 200, this means sent
              // code is valid
              NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
              int code = [httpResponse statusCode];
              if (code == 200) {
                [subscriber sendNext:@(YES)];
              } else {
                // Else, get error message
                NSError* jsonError;
                NSDictionary* dict = [NSJSONSerialization
                    JSONObjectWithData:data
                               options:NSJSONReadingMutableContainers
                                 error:&jsonError];
                if (jsonError) {
                  [subscriber sendError:jsonError];
                } else {
                  NSDictionary* errorResp =
                      [dict verifiedObjectForKey:@"error"];
                  if (errorResp) {
                    NSError* error = [NSError
                        errorWithDomain:ERROR_DOMAIN
                                   code:ERROR_CODE
                               userInfo:[errorResp
                                            verifiedObjectForKey:@"message"]];
                    [subscriber sendError:error];
                  }
                  [subscriber sendCompleted];
                }
              }
            }
          }];
    [task resume];

    return [RACDisposable disposableWithBlock:^{
      [task cancel];
    }];
  }];
  return signal;
}

@end
