//
//  SSSScanViewController.m
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "SSSScanViewController.h"
#import "SSSWebService.h"
#import <SVProgressHUD.h>
#import "SSSMessageModalViewController.h"
#import "NSDictionary+Verified.h"
#import <ReactiveCocoa.h>

@interface SSSScanViewController ()
@property(nonatomic) NSNumber* isValidating;
@property(strong, nonatomic) AVCaptureDevice* device;
@property(strong, nonatomic) AVCaptureDeviceInput* input;
@property(strong, nonatomic) AVCaptureMetadataOutput* output;
@property(strong, nonatomic) AVCaptureSession* session;
@property(strong, nonatomic) AVCaptureVideoPreviewLayer* previewLayer;
@property(strong, nonatomic) UIView* highlightView;
@end

@implementation SSSScanViewController

- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self updatePreviewFrame];
}

- (void)viewDidLoad {
  [super viewDidLoad];
  self.isValidating = [NSNumber numberWithInt:IS_SCANNER_NOT_VALIDATING];
  /// Check whether the camera is available or not, if availabe, setup it
  if ([self isCameraAvailable]) {
    [self setupScanner];
  }
  [self setupHighlightView];
  [self monitorRealtimePreviewSession];
}

- (void)viewDidDisappear:(BOOL)animated {
  self.highlightView.frame = CGRectZero;
}

- (void)dealloc {
  if ([self isCameraAvailable]) {
    [self stopScanning];
  }
}
- (void)didReceiveMemoryWarning {
  [super didReceiveMemoryWarning];
  // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark AVFoundation Configuration
- (void)setupScanner {
  self.device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];

  NSError* error;
  self.input =
      [AVCaptureDeviceInput deviceInputWithDevice:self.device error:&error];
  if (error) {
    // Display Error message
  }
  self.output = [[AVCaptureMetadataOutput alloc] init];
  self.session = [[AVCaptureSession alloc] init];
  [self.session addOutput:self.output];
  [self.session addInput:self.input];

  dispatch_queue_t dispatchQueue;
  dispatchQueue = dispatch_queue_create("Scan Queue", NULL);
  [self.output setMetadataObjectsDelegate:self queue:dispatchQueue];
  [self.output
      setMetadataObjectTypes:
          [NSArray arrayWithObjects:AVMetadataObjectTypeQRCode,
                                    AVMetadataObjectTypeEAN13Code,
                                    AVMetadataObjectTypeUPCECode,
                                    AVMetadataObjectTypeCode39Code,
                                    AVMetadataObjectTypeCode39Mod43Code,
                                    AVMetadataObjectTypeEAN13Code,
                                    AVMetadataObjectTypeEAN8Code,
                                    AVMetadataObjectTypeCode93Code,
                                    AVMetadataObjectTypeCode128Code,
                                    AVMetadataObjectTypePDF417Code,
                                    AVMetadataObjectTypeAztecCode, nil]];

  AVCaptureConnection* avConnection = self.previewLayer.connection;
  // Set Video Orientation based on the current device orientation
  switch (self.interfaceOrientation) {
    case UIInterfaceOrientationPortrait:
      avConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
      break;
    case UIInterfaceOrientationLandscapeLeft:
      avConnection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
      break;
    case UIInterfaceOrientationLandscapeRight:
      avConnection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
      break;
    case UIInterfaceOrientationPortraitUpsideDown:
      avConnection.videoOrientation =
          AVCaptureVideoOrientationPortraitUpsideDown;
      break;
    default:
      avConnection.videoOrientation = AVCaptureVideoOrientationPortrait;
      break;
  }

  self.previewLayer =
      [AVCaptureVideoPreviewLayer layerWithSession:self.session];
  self.previewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
  [self.previewLayer setFrame:self.viewPreview.bounds];
  [self.viewPreview.layer addSublayer:self.previewLayer];
  [self.viewPreview bringSubviewToFront:self.toolbar];
  [self.viewPreview bringSubviewToFront:self.guideTipLabel];
}

- (void)setupHighlightView {
  self.highlightView = [[UIView alloc] init];
  self.highlightView.autoresizingMask = UIViewAutoresizingFlexibleTopMargin |
                                        UIViewAutoresizingFlexibleLeftMargin |
                                        UIViewAutoresizingFlexibleRightMargin |
                                        UIViewAutoresizingFlexibleBottomMargin;
  self.highlightView.layer.borderColor = [UIColor greenColor].CGColor;
  self.highlightView.layer.borderWidth = 3;
  [self.viewPreview addSubview:self.highlightView];
  [self.viewPreview bringSubviewToFront:self.highlightView];
}

- (void)monitorRealtimePreviewSession {
  [[RACObserve(self, isValidating) filter:^BOOL(NSNumber* validateStatus) {
    return (validateStatus != nil);
  }] subscribeNext:^(NSNumber* status) {
    if ([status intValue] == IS_SCANNER_VALIDATING) {
      [self.session stopRunning];
    }
    if ([status intValue] == IS_SCANNER_NOT_VALIDATING) {
      [self.session startRunning];
    }
  }];
}

- (void)destroyScanner {
  self.session = nil;
  self.device = nil;
  [self.previewLayer removeFromSuperlayer];
}

#pragma mark -
#pragma mark Scan Actions
- (void)startScanning {
  [self.session startRunning];
}

- (void)stopScanning {
  [self.session stopRunning];
  [self destroyScanner];
}

#pragma mark -
#pragma mark AVCaptureMetadataOutputObjectsDelegate
- (void)captureOutput:(AVCaptureOutput*)captureOutput
    didOutputMetadataObjects:(NSArray*)metadataObjects
              fromConnection:(AVCaptureConnection*)connection {
  CGRect highlightViewRect = CGRectZero;
  AVMetadataMachineReadableCodeObject* readableCodeObject;

  if (metadataObjects != nil && metadataObjects.count > 0 &&
      (self.isValidating ==
       [NSNumber numberWithInt:IS_SCANNER_NOT_VALIDATING])) {
    for (AVMetadataObject* metaObj in metadataObjects) {
      if ([metaObj isKindOfClass:[AVMetadataMachineReadableCodeObject class]]) {
        NSString* scannedValue =
            [((AVMetadataMachineReadableCodeObject*)metaObj)stringValue];

        readableCodeObject =
            (AVMetadataMachineReadableCodeObject*)[self.previewLayer
                transformedMetadataObjectForMetadataObject:
                    (AVMetadataMachineReadableCodeObject*)metaObj];
        highlightViewRect = readableCodeObject.bounds;

        [self processQRCode:scannedValue];
        break;
      }
    }
  }
  dispatch_async(dispatch_get_main_queue(), ^{
    // Update Highlight Frame
    self.highlightView.frame = highlightViewRect;
  });
}

#pragma mark -
#pragma mark - SSSScanViewControllerDelegate
- (void)processQRCode:(NSString*)resultedQRStr {
  if (resultedQRStr != nil) {
    self.isValidating = [NSNumber numberWithInt:IS_SCANNER_VALIDATING];
    // Send to server
    [[[[SSSWebService sharedService] scanWithCode:resultedQRStr]
        deliverOn:[RACScheduler mainThreadScheduler]]
        subscribeNext:^(NSNumber* result) {
          BOOL isSucceed = [result boolValue];
          if (isSucceed) {
            [self showMessageVCWithStatus:YES errorMessage:nil animated:YES];
          }
          self.isValidating =
              [NSNumber numberWithInt:IS_SCANNER_NOT_VALIDATING];
        }
        error:^(NSError* error) {
          [self showMessageVCWithStatus:NO
                           errorMessage:[NSString
                                            stringWithFormat:@"%@",
                                                             [error userInfo]]
                               animated:YES];
          self.isValidating =
              [NSNumber numberWithInt:IS_SCANNER_NOT_VALIDATING];
        }];
  }
}

#pragma mark -
#pragma mark Helper Methods
- (BOOL)isCameraAvailable {
  NSArray* videoDevices =
      [AVCaptureDevice devicesWithMediaType:AVMediaTypeVideo];
  return [videoDevices count] > 0;
}

- (void)showMessageVCWithStatus:(BOOL)isSucceed
                   errorMessage:(NSString*)message
                       animated:(BOOL)animated {
  NSDictionary* data;
  if (isSucceed) {
    data = @{ @"status" : [NSNumber numberWithBool:isSucceed] };
  } else {
    data = @{
      @"status" : [NSNumber numberWithBool:isSucceed],
      @"message" : message
    };
  }

  [self performSegueWithIdentifier:@"showMessageModal" sender:data];
}

- (void)updatePreviewFrame {
  // Update preview frame
  [self.previewLayer setFrame:self.viewPreview.bounds];

  if ([[UIDevice currentDevice] orientation] ==
      UIDeviceOrientationLandscapeLeft) {
    AVCaptureConnection* con = self.previewLayer.connection;
    con.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
  }
  if ([[UIDevice currentDevice] orientation] ==
      UIDeviceOrientationLandscapeRight) {
    AVCaptureConnection* con = self.previewLayer.connection;
    con.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
  }
  if ([[UIDevice currentDevice] orientation] == UIDeviceOrientationPortrait) {
    AVCaptureConnection* con = self.previewLayer.connection;
    con.videoOrientation = AVCaptureVideoOrientationPortrait;
  }
  if ([[UIDevice currentDevice] orientation] ==
      UIDeviceOrientationPortraitUpsideDown) {
    AVCaptureConnection* con = self.previewLayer.connection;
    con.videoOrientation = AVCaptureVideoOrientationPortraitUpsideDown;
  }
}

#pragma mark -
#pragma mark Orientation Supportation
- (NSUInteger)supportedInterfaceOrientations;
{ return UIInterfaceOrientationMaskLandscape; }

- (BOOL)shouldAutorotate;
{
  return (
      UIDeviceOrientationIsLandscape([[UIDevice currentDevice] orientation]));
}

- (void)didRotateFromInterfaceOrientation:
        (UIInterfaceOrientation)fromInterfaceOrientation;
{ [self updatePreviewFrame]; }

#pragma mark -
#pragma mark Segues
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([segue.identifier isEqual:@"showMessageModal"]) {
    SSSMessageModalViewController* messageVC =
        (SSSMessageModalViewController*)segue.destinationViewController;
    if (sender != nil && [sender isKindOfClass:[NSDictionary class]]) {
      NSDictionary* data = (NSDictionary*)sender;
      NSNumber* status = [data verifiedObjectForKey:@"status"];
      NSString* message = [data verifiedObjectForKey:@"message"];
      if (status) {
        [messageVC setIsSucceed:status];
      }
      if (message) {
        [messageVC setMessage:message];
      }
    }
  }
}
@end
