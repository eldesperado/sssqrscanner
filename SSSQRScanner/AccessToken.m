//
//  AccessToken.m
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "AccessToken.h"
#import <SSKeychain/SSKeychain.h>
#import "User.h"
#import "NSDictionary+Verified.h"
#import "NSData+Base64.h"

@implementation AccessToken
#pragma mark Accessors
@synthesize accessToken;

+ (id)tokenWithResponseBody:(NSDictionary*)json {
  NSString* anAccessToken = [json verifiedObjectForKey:@"access_token"];
  return [[[self class] alloc] initWithAccessToken:anAccessToken];
}

#pragma mark -
#pragma mark - Initialize
- (id)initWithAccessToken:(NSString*)anAccessToken {
  if (anAccessToken == nil) {
    return nil;
  }
  self = [super init];
  if (self) {
    accessToken = [anAccessToken copy];
  }
  return self;
}

#pragma mark -
#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder*)aCoder {
  [aCoder encodeObject:accessToken forKey:@"accessToken"];
}

- (id)initWithCoder:(NSCoder*)aDecoder {
  NSString* decodedAccessToken = [aDecoder decodeObjectForKey:@"accessToken"];

  // a token object without an actual token is not what we want!
  if (decodedAccessToken == nil) {
    return nil;
  }

  self = [super init];
  if (self) {
    accessToken = [decodedAccessToken copy];
  }
  return self;
}

#pragma mark -
#pragma mark Keychain Support

+ (NSString*)serviceName;
{
  NSString* appName = [[NSBundle mainBundle] bundleIdentifier];

  return [NSString stringWithFormat:@"%@::OAuth2", appName];
}

+ (NSString*)accountName;
{
  NSString* name = nil;
  User* user = [User MR_findFirst];
  if (user != nil) {
    name = user.username;
  }
  return name;
}

+ (id)tokenFromDefaultKeychain {
  NSString* serviceName = [[self class] serviceName];
  NSString* account = [[self class] accountName];
  NSError* error;
  NSString* dataString =
      [SSKeychain passwordForService:serviceName account:account error:&error];
  if (error) {
    return nil;
  }
  NSData* data = [NSData dataFromBase64String:dataString];

  return [NSKeyedUnarchiver unarchiveObjectWithData:data];
}

- (void)storeInDefaultKeychain {
  NSString* serviceName = [[self class] serviceName];
  NSString* account = [[self class] accountName];
  NSData* data = [NSKeyedArchiver archivedDataWithRootObject:self];

  // Create a Base-64 encoded NSString from Token
  NSString* dataString = [data base64EncodedString];
  NSError* error;
  [SSKeychain setPassword:dataString
               forService:serviceName
                  account:account
                    error:&error];
  if (error) {
  }
}
- (void)removeFromDefaultKeychain {
  NSString* serviceName = [[self class] serviceName];
  NSString* account = [[self class] accountName];
  NSError* error;
  [SSKeychain deletePasswordForService:serviceName
                               account:account
                                 error:&error];
  if (error) {
    [error localizedDescription];
  }
}
@end
