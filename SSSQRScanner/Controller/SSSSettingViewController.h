//
//  SSSSettingViewController.h
//  SSSQRScanner
//
//  Created by El Desperado on 31/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <TTTAttributedLabel.h>

@interface SSSSettingViewController : UIViewController<UIAlertViewDelegate>
@property(strong, nonatomic) IBOutlet TTTAttributedLabel* usernameLabel;

- (IBAction)logoutAction:(id)sender;

@end
