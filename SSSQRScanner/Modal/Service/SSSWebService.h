//
//  SSSWebService.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveCocoa.h>
#import "User.h"

@class RACSignal;

@interface SSSWebService : NSObject<NSURLSessionDelegate>
@property(nonatomic, copy) NSURL* baseUrl;
@property(nonatomic, copy) NSURLSession* client;
+ (SSSWebService*)sharedService;
- (RACSignal*)scanWithCode:(NSString*)code;
@end
