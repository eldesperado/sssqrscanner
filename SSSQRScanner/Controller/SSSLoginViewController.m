//
//  SSSLoginViewController.m
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "SSSLoginViewController.h"
#import <ReactiveCocoa/ReactiveCocoa.h>
#import "AuthenticationClient.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "AppDelegate.h"

@interface SSSLoginViewController () {
  UIViewController* scanVC;
  UITextField* activeField;
}

@end
@implementation SSSLoginViewController
@synthesize signInBTN = _signInBTN;

- (void)viewDidLoad {
  [super viewDidLoad];
  scanVC = [[UIStoryboard
      storyboardWithName:@"Main"
                  bundle:[NSBundle
                                 mainBundle]] instantiateInitialViewController];
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  [self registerForKeyboardNotifications];
}
- (void)viewDidDisappear:(BOOL)animated {
  [super viewDidDisappear:animated];
  [self unregisterForKeyboardNotifications];
}

#pragma mark -
#pragma mark Actions
- (void)showScanVC {
  AppDelegate* appDelegateTemp = [[UIApplication sharedApplication] delegate];
  appDelegateTemp.window.rootViewController = scanVC;
}

- (IBAction)signInBTNTappedAction:(id)sender {
  self.signInBTN.enabled = NO;
  [SVProgressHUD show];
  [[[[AuthenticationClient sharedInstance]
      authenticateWithUsername:self.usernameTextField.text
                      password:self.passwTextField.text]
      deliverOn:RACScheduler.mainThreadScheduler]
      subscribeNext:^(NSNumber* signedIn) {
        self.signInBTN.enabled = YES;
        BOOL isSucceedLogin = [signedIn boolValue];
        // Dimiss Login View if logged-in successfully
        if (isSucceedLogin) {
          [self showScanVC];
        }
      }
      error:^(NSError* error) {
        [self showErrorMessageWithError:error];
        self.signInBTN.enabled = YES;
      }];
}

- (IBAction)bgTappedAction:(id)sender {
  [self.view endEditing:YES];
}

#pragma mark -
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  NSInteger nextTag = textField.tag + 1;
  // Try to find next responder to navigate through TextFields
  UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
  if (nextResponder) {
    // Found next responder, so set it.
    [nextResponder becomeFirstResponder];
  } else {
    // Not found, so remove keyboard.
    [textField resignFirstResponder];
    [self signInBTNTappedAction:self];
  }
  return NO;
}

#pragma mark -
#pragma mark Action Helpers
- (void)showErrorMessageWithError:(NSError*)error {
  if ([error localizedDescription]) {
    [SVProgressHUD
        showErrorWithStatus:
            [NSString stringWithFormat:@"%@", [error localizedDescription]]
                   maskType:SVProgressHUDMaskTypeBlack];
  } else {
    [SVProgressHUD showErrorWithStatus:@"Login Failed!"];
  }
}

#pragma mark - Keyboard Helpers
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications {
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWasShown:)
             name:UIKeyboardDidShowNotification
           object:nil];

  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWillBeHidden:)
             name:UIKeyboardWillHideNotification
           object:nil];
}

// unregister for keyboard notifications while not visible.
- (void)unregisterForKeyboardNotifications {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillShowNotification
              object:nil];

  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillHideNotification
              object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
  if ([[UIDevice currentDevice] userInterfaceIdiom] ==
      UIUserInterfaceIdiomPhone) {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize =
        [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat kbHeight;
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
      kbHeight = kbSize.height;
    } else {
      kbHeight = kbSize.width;
    }

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbHeight, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbHeight;
    if (!CGRectContainsPoint(aRect, activeField.frame.origin)) {
      [self.scrollView scrollRectToVisible:activeField.frame animated:YES];
    }
  }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
  self.scrollView.contentInset = contentInsets;
  self.scrollView.scrollIndicatorInsets = contentInsets;
}

- (void)textFieldDidBeginEditing:(UITextField*)textField {
  activeField = textField;
}

- (void)textFieldDidEndEditing:(UITextField*)textField {
  activeField = nil;
}

@end
