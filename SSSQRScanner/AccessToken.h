//
//  AccessToken.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Security/Security.h>

@interface AccessToken : NSObject<NSCoding> {
 @private
  NSString* accessToken;
}
@property(nonatomic, readonly) NSString* accessToken;

+ (id)tokenWithResponseBody:(NSDictionary*)responseBody;
- (id)initWithAccessToken:(NSString*)accessToken;

#pragma mark Keychain Support
+ (id)tokenFromDefaultKeychain;
- (void)storeInDefaultKeychain;
- (void)removeFromDefaultKeychain;
@end
