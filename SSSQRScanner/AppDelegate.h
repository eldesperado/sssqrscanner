//
//  AppDelegate.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <CoreData+MagicalRecord.h>

@interface AppDelegate : UIResponder<UIApplicationDelegate>

@property(strong, nonatomic) UIWindow* window;

- (NSURL*)applicationDocumentsDirectory;

@end
