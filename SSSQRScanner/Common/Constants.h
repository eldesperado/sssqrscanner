//
//  Constants.h
//  SSSQRScanner
//
//  Created by El Desperado on 30/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#ifndef SSSQRScanner_Constants_h
#define SSSQRScanner_Constants_h

#define WEB_SERVER_URL @"https://sss-mobile-test.herokuapp.com"
#define ERROR_DOMAIN @"com.sss.ErrorDomain"
#define ERROR_CODE 381
#define NSUSERDEFAULTS_LOGGEDIN_KEY @"userLoggedIn"
#define kOFFSET_FOR_KEYBOARD 80.0f
#define IS_SCANNER_VALIDATING 1
#define IS_SCANNER_NOT_VALIDATING 0

#endif
