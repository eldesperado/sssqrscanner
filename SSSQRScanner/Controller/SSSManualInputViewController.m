//
//  SSSManualInputViewController.m
//  SSSQRScanner
//
//  Created by El Desperado on 31/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "SSSManualInputViewController.h"
#import <SVProgressHUD/SVProgressHUD.h>
#import "SSSWebService.h"
#import "SSSMessageModalViewController.h"
#import "NSDictionary+Verified.h"

@implementation SSSManualInputViewController
- (void)viewDidLoad {
}
- (void)viewWillAppear:(BOOL)animated {
  [super viewWillAppear:animated];
  // register for keyboard notifications
  [self registerForKeyboardNotifications];
}

- (void)viewWillDisappear:(BOOL)animated {
  [super viewWillDisappear:animated];
  [self unregisterForKeyboardNotifications];
}

#pragma mark -
#pragma mark - Actions
- (IBAction)bgTappedAction:(id)sender {
  [self.view endEditing:YES];
}

- (IBAction)sendCodeAction:(id)sender {
  [SVProgressHUD show];
  [[[[SSSWebService sharedService] scanWithCode:self.codeInputTextField.text]
      deliverOn:RACScheduler
                    .mainThreadScheduler] subscribeNext:^(NSNumber* signedIn) {
    self.sendBTN.enabled = YES;
    BOOL isSucceed = [signedIn boolValue];
    // Dimiss Login View if logged-in successfully
    if (isSucceed) {
      [SVProgressHUD dismiss];
      [self showMessageVCWithStatus:YES errorMessage:nil animated:YES];
    }
  } error:^(NSError* error) {
    [SVProgressHUD dismiss];
    [self showMessageVCWithStatus:NO
                     errorMessage:[NSString
                                      stringWithFormat:@"%@", [error userInfo]]
                         animated:YES];
  }];
}

- (void)showMessageVCWithStatus:(BOOL)isSucceed
                   errorMessage:(NSString*)message
                       animated:(BOOL)animated {
  NSDictionary* data;
  if (isSucceed) {
    data = @{ @"status" : [NSNumber numberWithBool:isSucceed] };
  } else {
    data = @{
      @"status" : [NSNumber numberWithBool:isSucceed],
      @"message" : message
    };
  }

  [self performSegueWithIdentifier:@"showMessageModal" sender:data];
  self.codeInputTextField.text = @"";
}

#pragma mark -
#pragma mark Segues
- (void)prepareForSegue:(UIStoryboardSegue*)segue sender:(id)sender {
  if ([segue.identifier isEqual:@"showMessageModal"]) {
    SSSMessageModalViewController* messageVC =
        (SSSMessageModalViewController*)segue.destinationViewController;
    if (sender != nil && [sender isKindOfClass:[NSDictionary class]]) {
      NSDictionary* data = (NSDictionary*)sender;
      NSNumber* status = [data verifiedObjectForKey:@"status"];
      NSString* message = [data verifiedObjectForKey:@"message"];
      if (status) {
        [messageVC setIsSucceed:status];
      }
      if (message) {
        [messageVC setMessage:message];
      }
    }
  }
}

#pragma mark -
#pragma mark - UITextFieldDelegate
- (BOOL)textFieldShouldReturn:(UITextField*)textField {
  // Remove keyboard.
  [textField resignFirstResponder];
  [self sendCodeAction:nil];
  return NO;
}

#pragma mark - Keyboard Helpers
// Call this method somewhere in your view controller setup code.
- (void)registerForKeyboardNotifications {
  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWasShown:)
             name:UIKeyboardDidShowNotification
           object:nil];

  [[NSNotificationCenter defaultCenter]
      addObserver:self
         selector:@selector(keyboardWillBeHidden:)
             name:UIKeyboardWillHideNotification
           object:nil];
}

// unregister for keyboard notifications while not visible.
- (void)unregisterForKeyboardNotifications {
  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillShowNotification
              object:nil];

  [[NSNotificationCenter defaultCenter]
      removeObserver:self
                name:UIKeyboardWillHideNotification
              object:nil];
}

// Called when the UIKeyboardDidShowNotification is sent.
- (void)keyboardWasShown:(NSNotification*)aNotification {
  if ([[UIDevice currentDevice] userInterfaceIdiom] ==
      UIUserInterfaceIdiomPhone) {
    NSDictionary* info = [aNotification userInfo];
    CGSize kbSize =
        [[info objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    CGFloat kbHeight;
    if (self.interfaceOrientation == UIInterfaceOrientationPortrait) {
      kbHeight = kbSize.height;
    } else {
      kbHeight = kbSize.width;
    }

    UIEdgeInsets contentInsets = UIEdgeInsetsMake(0.0, 0.0, kbHeight, 0.0);
    self.scrollView.contentInset = contentInsets;
    self.scrollView.scrollIndicatorInsets = contentInsets;

    // If active text field is hidden by keyboard, scroll it so it's visible
    // Your app might not need or want this behavior.
    CGRect aRect = self.view.frame;
    aRect.size.height -= kbHeight;
    if (!CGRectContainsPoint(aRect, self.codeInputTextField.frame.origin)) {
      [self.scrollView scrollRectToVisible:self.codeInputTextField.frame
                                  animated:YES];
    }
  }
}

// Called when the UIKeyboardWillHideNotification is sent
- (void)keyboardWillBeHidden:(NSNotification*)aNotification {
  UIEdgeInsets contentInsets = UIEdgeInsetsZero;
  self.scrollView.contentInset = contentInsets;
  self.scrollView.scrollIndicatorInsets = contentInsets;
}

@end
