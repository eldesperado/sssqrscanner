//
//  SSSMessageModalViewController.h
//  SSSQRScanner
//
//  Created by El Desperado on 31/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SSSMessageModalViewController : UIViewController
@property(strong, nonatomic) IBOutlet UILabel* statusLabel;
@property(strong, nonatomic) IBOutlet UIImageView* iconImageView;
@property(strong, nonatomic) IBOutlet UILabel* messageLabel;
@property(strong, nonatomic) NSNumber* isSucceed;
@property(strong, nonatomic) NSString* message;
- (IBAction)didTappedAction:(id)sender;

@end
