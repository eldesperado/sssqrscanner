//
//  SSSSettingViewController.m
//  SSSQRScanner
//
//  Created by El Desperado on 31/01/2015.
//  Copyright (c) Năm 2015 El Desperado. All rights reserved.
//

#import "SSSSettingViewController.h"
#import "SSSLoginViewController.h"
#import "AuthenticationClient.h"
#import "AppDelegate.h"
#import "User.h"

@interface SSSSettingViewController () {
  UIViewController* loginVC;
}
@end

@implementation SSSSettingViewController

- (void)viewDidLoad {
  [self loadUserName];
  loginVC =
      [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]]
          instantiateViewControllerWithIdentifier:@"LoginViewController"];
}

#pragma mark -
#pragma mark View Configuration
- (void)loadUserName {
  User* user = [User MR_findFirst];
  [self setUsernameLabelWithString:[NSString stringWithFormat:@"Login as: %@",
                                                              user.username]];
}

- (void)setUsernameLabelWithString:(NSString*)usernameStr {
  [self.usernameLabel setText:usernameStr
      afterInheritingLabelAttributesAndConfiguringWithBlock:
          ^NSMutableAttributedString*(
              NSMutableAttributedString* mutableAttributedString) {
            NSRange placeHolderRange = [[mutableAttributedString string]
                rangeOfString:@"Login as: "
                      options:NSCaseInsensitiveSearch];
            NSRange boldRange =
                NSMakeRange(placeHolderRange.location + placeHolderRange.length,
                            usernameStr.length - placeHolderRange.length);

            UIFont* boldSystemFont =
                [UIFont fontWithName:@"HelveticaNeue-Medium" size:17.0f];
            CTFontRef font = CTFontCreateWithName(
                (__bridge CFStringRef)boldSystemFont.fontName,
                boldSystemFont.pointSize, NULL);
            if (font) {
              [mutableAttributedString
                  addAttribute:(NSString*)kCTFontAttributeName
                         value:(__bridge id)font
                         range:boldRange];
              CFRelease(font);
            }
            [mutableAttributedString
                addAttribute:(NSString*)kCTForegroundColorAttributeName
                       value:[UIColor colorWithRed:0.498
                                             green:0.549
                                              blue:0.553
                                             alpha:1.000]
                       range:placeHolderRange];
            return mutableAttributedString;
          }];
}

#pragma mark -
#pragma mark - Actions
- (IBAction)logoutAction:(id)sender {
  [self showConfirmationPopup];
}

- (void)showLoginForm {
  AppDelegate* appDelegateTemp = [[UIApplication sharedApplication] delegate];
  appDelegateTemp.window.rootViewController = loginVC;
}

- (void)showConfirmationPopup {
  UIAlertView* alert =
      [[UIAlertView alloc] initWithTitle:@"Confirmation"
                                 message:@"Are you sure to log out?"
                                delegate:self
                       cancelButtonTitle:@"No"
                       otherButtonTitles:@"Yes", nil];
  [alert show];
}

#pragma mark -
#pragma mark UIAlertViewDelegate
- (void)alertView:(UIAlertView*)alertView
    clickedButtonAtIndex:(NSInteger)buttonIndex {
  switch (buttonIndex) {
    case 0:  //"No" pressed
      break;
    case 1:  //"Yes" pressed
      if ([[AuthenticationClient sharedInstance] isLoggedIn]) {
        [[AuthenticationClient sharedInstance] logout];
        [self showLoginForm];
      }
      break;
  }
}
@end
